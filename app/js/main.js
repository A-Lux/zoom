$(function () {
    let body = $('.content_inner')
    var position = 0
    var waiting = false
    var windowSize = false
    let svgs = $('svg')
    $('.content_inner').load('main.html')
    let whatsapp = document.querySelector(".section_whatsapp img")

    var array = [
      {
        template: 'main.html'
      },
      {
        template: 'about.html'
      },
      {
        template: 'steps.html'
      },
      {
        template: 'cases.html'
      },
      {
        template: 'contacts.html'
      }
    ]
    let curPos = 0
    function myFunction(x) {
      if (x.matches) { 
        windowSize = true
      }
    }
    document.body.addEventListener('wheel',function(e) {
      if(waiting || windowSize) return;
      let position = e.deltaY
      if(curPos === 3) return;
      
      if(position > 0){
        let after=curPos
        if(curPos >= array.length) return;
        curPos++
        svgs[curPos].style.fill = "#0055f9"
        svgs[after].style.fill = "#cccccc"
      }
      else{
        let top=curPos
        if(curPos <= 0) return;
        curPos--
        svgs[top].style.fill = "#cccccc"
        svgs[curPos].style.fill = "#0055f9"
      }
      $('.content_inner').load(array[curPos].template)
      waiting = true;
      e.preventDefault()
      callTheWaiting()
    },
    {
      passive:false
    });
    function callTheWaiting(){
      
      callBackground()
      setTimeout(()=>{
        waiting = false
      },1000)
    }
    
    for(let x = 0;x < svgs.length;x++){
      svgs[x].addEventListener('click',function(){
        svgs[curPos].style.fill = "#cccccc"
        svgs[x].style.fill = "#0055f9"
        $('.content_inner').load(array[x].template)
        curPos = x
        callBackground()
      })
    }

    function callBackground(){
      if(curPos === 1)  $('.all_content')[0].classList.add('background-class')
      else $('.all_content')[0].classList.remove('background-class')
    }

    var x = window.matchMedia("(max-width: 680px)")
    myFunction(x) 
    x.addListener(myFunction)




    
});